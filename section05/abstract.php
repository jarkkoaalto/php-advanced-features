<?php

abstract class Cars {
    protected $brand = 'BMW';

    public function showBrand(){
        return $this->brand;
    }

    abstract public function hideBrand($brand);
}

class BMW extends Cars{
    public function displayData(){
        return $this->showBrand();
    }

    public function hideBrand($brand)
    {
        echo $brand;
    }
}

echo (new BMW())->displayData();
echo "<br>";
(new BMW())->hideBrand("AUDI");
?>