<?php
interface DisplayInterface {
    public function displayDataInterface(array $data);
}


class Display implements displayInterface{
    public function displayDataInterface(array $data){
        echo "I am doing anything I want here!" . " ". $data['name'];
    }
}

$display = new Display();

$display->displayDataInterface(['name'=>'Joe']);

?>