<?php

trait Display {
    protected function showThis($message){
        echo $message;
    }
}

trait SaveData {
    protected function SaveThis($message){
        return $message;
    }
}


class Main{

    use Display;
    use saveData;
    public function displayingData(){
        $this->showThis('HELLO THERE, from main using traits');
    }

    public function SavingData(){
        return $this->saveThis('Saving Data');
    }
}

(new Main()) -> displayingData();
echo "<br>";
echo (new Main()) -> SavingData();

?>