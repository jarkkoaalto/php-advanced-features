# PHP Advanced Features #

## Section 01 Introduction ##

## Section 02 Namespaces ##

What are Namespaces?

Professional Answer:
- Namespaces are used to organize code into logical groups and to prevent name collisions that can occur especially when your code base includes multiple libraries.

- Namespaves are used to organize code but also to be able to use muliple classes with the same name.

### Examples ###

Path:
- /app/helpers/display.php
- /libraries/utilies/display.php

Namespace:
- \App\Helpers\Display
- \App\Utilities\Display

# Section 03 #

## Interfaces ##

What are Interfaces?

Answer?
- Contains no data or code, but defines behaviors as method signatures.

### Example ###

```
interface DisplayInterface {
    public function displayData(array $data); \\Method signature
}
```

# Section 04 #
## Traits ##

Traits Overview 

Answer:
- Traits are a mechanism for code reuse. Basically you can use pieces of code in several independent classes.

### Example ###

```
trait DisplayTrait {
    protected function displayTraitMethod($message){
        return $message;
    }
}
```

What are they good for?
- Traits are good when you need reuse code that are located in multiple different files

PHP is a single Inheritance Language
- You can only inherit from on class

# Section 05 #
## Abstract Classes ##

Abstarct Classes Overview
 
 What are Abstarct Classes and Methods?

 Answer:
 - Classes that provide base code for children classes to implement or override.

 ### Declaration ###

 ```
abstract Cars {

    protected function showBrand(){
        return $this->brand;
    }
}
 ```

What are they good for?

They are good when you don't want to procide an instance but implementation to the children

